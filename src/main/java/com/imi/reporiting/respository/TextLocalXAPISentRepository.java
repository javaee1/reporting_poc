package com.imi.reporiting.respository;

import org.springframework.data.repository.CrudRepository;

import com.imi.reporiting.entity.TextLocalXAPISent;

public interface TextLocalXAPISentRepository extends CrudRepository<TextLocalXAPISent, Long>{

}
