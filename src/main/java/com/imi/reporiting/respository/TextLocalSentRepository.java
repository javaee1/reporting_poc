package com.imi.reporiting.respository;

import org.springframework.data.repository.CrudRepository;

import com.imi.reporiting.entity.TextLocalSent;

public interface TextLocalSentRepository extends CrudRepository<TextLocalSent, Long>{

}
