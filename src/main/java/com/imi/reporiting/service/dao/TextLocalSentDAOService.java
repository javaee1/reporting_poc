package com.imi.reporiting.service.dao;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imi.reporiting.entity.TextLocalSent;
import com.imi.reporiting.respository.TextLocalSentRepository;

@Service
public class TextLocalSentDAOService {
	
	@Autowired
	private TextLocalSentRepository textlocalSentReopository;
	
	
	@Transactional
	public TextLocalSent findTextLocalSentById(long id) {
		Optional<TextLocalSent> optionalObj = textlocalSentReopository.findById(id);
		if(optionalObj.isPresent()) {
			TextLocalSent obj = optionalObj.get();
			obj.setTextLocalXAPISentList(obj.getTextLocalXAPISentList());
			return obj;
		}
		return null;
	}
	
	@Transactional
	public TextLocalSent runningGoal(long id) {
		Optional<TextLocalSent> optionalObj = textlocalSentReopository.findById(id);
		if(optionalObj.isPresent()) {
			TextLocalSent obj = optionalObj.get();
			obj.getTextLocalXAPISentList();
			return obj;
			
		}
		return null;
	}
	
	
}
