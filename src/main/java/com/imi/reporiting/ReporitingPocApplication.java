package com.imi.reporiting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReporitingPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReporitingPocApplication.class, args);
	}

}
