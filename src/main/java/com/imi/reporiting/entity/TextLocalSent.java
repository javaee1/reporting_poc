package com.imi.reporiting.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tl_sent")
public class TextLocalSent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	
	@Column(name = "userid")
    private Long userId;
	
	@OneToMany(mappedBy = "textLocalSent", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	Set<TextLocalXAPISent> textLocalXAPISentList;
	
	@Column(name = "date")
    private Date date;
	
    private String message;
	
    private float howmany;
	
	@Column(name = "groupid")
    private int groupId;
	
    private float totalcredits;

    private String sched;
	
    private String origin;
	
    private String url;
	
    private String ipaddress;
	
    private boolean processing;

	@Column(name = "groupMemberCount")
    private int groupMemberCount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public float getHowmany() {
		return howmany;
	}

	public void setHowmany(float howmany) {
		this.howmany = howmany;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public float getTotalcredits() {
		return totalcredits;
	}

	public void setTotalcredits(float totalcredits) {
		this.totalcredits = totalcredits;
	}

	public String getSched() {
		return sched;
	}

	public void setSched(String sched) {
		this.sched = sched;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public boolean isProcessing() {
		return processing;
	}

	public void setProcessing(boolean processing) {
		this.processing = processing;
	}

	public int getGroupMemberCount() {
		return groupMemberCount;
	}

	public void setGroupMemberCount(int groupMemberCount) {
		this.groupMemberCount = groupMemberCount;
	}

	@Override
	public String toString() {
		return "TextLocalSent [id=" + id + ", userId=" + userId + ", groupId=" + groupId + ", totalcredits="
				+ totalcredits + "]";
	}

	public Set<TextLocalXAPISent> getTextLocalXAPISentList() {
		System.out.print(textLocalXAPISentList);
		return textLocalXAPISentList;
	}

	public void setTextLocalXAPISentList(Set<TextLocalXAPISent> textLocalXAPISentList) {
		this.textLocalXAPISentList = textLocalXAPISentList;
	}
}
