package com.imi.reporiting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tl_xapisent")
public class TextLocalXAPISent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "number")
	private Long number;

	@Column(name = "origin")
	private String origin;

	@Column(name = "message")
	private String message;

	@Column(name = "status")
	private String status;
	
	
	@ManyToOne
	@JoinColumn(name="f_sent_id")
	private TextLocalSent textLocalSent;

	@Column(name = "customer")
	private Integer customer;
	
	@Column(name = "apiType")
	private Integer apiType;
 
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public Integer getCustomer() {
		return customer;
	}

	public void setCustomer(Integer customer) {
		this.customer = customer;
	}

	public Integer getApiType() {
		return apiType;
	}

	public void setApiType(Integer apiType) {
		this.apiType = apiType;
	}

	public TextLocalSent getTextLocalSent() {
		return textLocalSent;
	}

	public void setTextLocalSent(TextLocalSent textLocalSent) {
		this.textLocalSent = textLocalSent;
	}

	@Override
	public String toString() {
		return "SmppPendingLIVEIndia [id=" + id + ", number=" + number + ", origin=" + origin + ", customer=" + customer
				+ "]";
	}
	
}
